from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from query_wiki_app.views import index

urlpatterns = patterns('',
    url(r'^$', index, name='index'),
    url(r'^api/', include('api.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
