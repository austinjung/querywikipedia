from django.conf.urls import url, include, patterns
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register(r'wiki', views.QueryWikiView, base_name='wiki')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = patterns('api',
    url(r'^', include(router.urls)),
)
