from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.exceptions import ParseError
import urllib2
import urllib
import json
from xml.etree import ElementTree
import re

class QueryWikiView(viewsets.ViewSet):

    def __init__(self, *args, **kwargs):
        super(QueryWikiView, self).__init__(*args, **kwargs)
        proxy = urllib2.ProxyHandler({})
        auth = urllib2.HTTPBasicAuthHandler()
        opener = urllib2.build_opener(proxy, auth, urllib2.HTTPHandler)
        urllib2.install_opener(opener)
        self.wiki_proxy = urllib2

    def list(self, request):
        return Response(self.open_search_query())

    def open_search_query(self):
        format = self.request.query_params.get('format', 'json').lower()
        query = urllib.urlencode({
            'action': 'opensearch',
            'search': self.request.query_params.get('title', 'A'),
            'format': format
        })
        connection = self.wiki_proxy.urlopen('https://en.wikipedia.org/w/api.php?%s' % query)
        wiki_response = connection.read()
        if format == 'json':
            wiki_json_response = json.loads(wiki_response)
            json_response = []
            for i in range(0, len(wiki_json_response[1])):
                article = {
                    'Text': wiki_json_response[1][i],
                    'Description': wiki_json_response[2][i],
                    'Url': wiki_json_response[3][i]
                }
                if article['Description'] == '':
                    article['Description'] = article['Text']
                json_response.append(article)
            return json_response
        elif format == 'xml':
            wiki_xml_response = ElementTree.fromstring(wiki_response)
            xml_response = []
            for child in wiki_xml_response[1]:
                article = {}
                for attr in child:
                    article[re.sub(r'{.+}', '', attr.tag)] = attr.text
                if not article.get('Description'):
                    article['Description'] = article['Text']
                xml_response.append(article)
            return xml_response
        else:
            raise ParseError(detail='%s is not supported format.' % format)
